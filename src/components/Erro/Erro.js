import { Container } from "./styles"

const ErroCard = () => {
  return (
    <Container>
      <div className="row">
        <div className="card text-center col-sm col-lg-8">
          <h5 className="card-header bg-secondary text-light">Página Não Encontrada</h5>
          <div className="card-body bg-dark text-white text-center py-5">
            <h5 className="card-title">Tente Novamente!</h5>
            <p className="card-text">Retornando a página inicial</p>
            <a href="/" className="btn btn-warning">Home</a>
          </div>
        </div>
      </div>
    </Container>
  )
}

export default ErroCard

