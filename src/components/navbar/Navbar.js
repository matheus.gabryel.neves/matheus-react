import { Link } from "react-router-dom"

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link className="navbar-brand text-warning" to="/">
        MyTems
      </Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Alterna navegação">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item active">
            <Link className="nav-link text-warning" to="/">
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-warning" to="produtos">
              Produtos
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-warning" to="sobre">
              Sobre
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  )
}

export default Navbar

