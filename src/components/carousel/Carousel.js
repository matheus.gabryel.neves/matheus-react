import { Slider } from './styles';

const Carousel = () => {
  return (
    <Slider>
      <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="https://st2.depositphotos.com/6544740/9337/i/600/depositphotos_93376372-stock-photo-sunset-over-sea-pier.jpg" alt="Primeiro Slide" />
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="https://www.stoodi.com.br/wp-content/uploads/2018/11/como-se-escreve-por-do-sol.jpg" alt="Segundo Slide" />
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="https://lupadigital.info/wp-content/uploads/2018/05/imagens-gratis.jpg" alt="Terceiro Slide" />
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Próximo</span>
        </a>
      </div>
    </Slider>
  )
}

export default Carousel

