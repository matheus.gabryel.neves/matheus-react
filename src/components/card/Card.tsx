import { Container } from "./styles"


const Card = (props: any) => {
    return (
        <Container className="card bg-dark text-white">
            <img className="card-img-top" src={props.img} alt="Card image cap" />
            <div className="card-body">
                <h5 className="card-title">{props.title}</h5>
                <p className="card-text">{props.body}</p>
                <a href={props.link} className="btn btn-warning">clique</a>
            </div>
        </Container>
    )
}

export default Card