import styled from "styled-components"

const Sobre = () => {
    return (
        <Container>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-sm col-lg-8">
                        <div className="card text-center">

                            <h5 className="card-header bg-secondary text-light">Sobre Nós</h5>

                            <div className="card-body bg-dark text-white text-center py-5">
                                <h5 className="card-title">Lorem Ipsum</h5>
                                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lobortis consequat neque eu tempus. Curabitur eget dictum ex. Aliquam auctor blandit erat at posuere. Praesent in gravida lacus. Donec aliquam tempor arcu, ut vestibulum leo tristique quis. Orci varius natoque penatibus et magnis dis parturient montes</p>
                                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lobortis consequat neque eu tempus. Curabitur eget dictum ex. Aliquam auctor blandit erat at posuere. Praesent in gravida lacus. Donec aliquam tempor arcu, ut vestibulum leo tristique quis. Orci varius natoque penatibus et magnis dis parturient montes</p>
                                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lobortis consequat neque eu tempus. Curabitur eget dictum ex. Aliquam auctor blandit erat at posuere. Praesent in gravida lacus. Donec aliquam tempor arcu, ut vestibulum leo tristique quis. Orci varius natoque penatibus et magnis dis parturient montes</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    )
}

export default Sobre

export const Container = styled.div`
  height: 100vh;

  .card {
    margin-top: 30%;
  }
`