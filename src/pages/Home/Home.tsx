import Card from '../../components/card/Card';
import Carousel from '../../components/carousel';

const Home = () => {
  const dados = [
    {
      id: 1,
      title: "kkkkkkkkkk",
      body: "haaaaaaaaaaaaaaaaaaaa",
      link: "https://www.facebook.com/gustavo.soares.79",
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMtqoqrb0yq0B3xZblSXbQnbY3-HF-1RMw8A&usqp=CAU",
    },
    {
      id: 2,
      title: "golira aleatorio da internet",
      body: "um bufo",
      link: "https://cetesc.com.br",
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7zdbmyHTxW8y4p_z5Rsq0piCjF3trESMumw51P_3wrMrwCc2DaGUB4rcvif7ek6JDcXU&usqp=CAU",
    },
    {
      id: 3,
      title: "cachorro",
      body: "auau",
      link: "https://cetesc.com.br",
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfap069Yu47hKWFHqVgIEyz71U871mcLOwCg&usqp=CAU",
    },
    {
      id: 4,
      title: "lobo",
      body: "morto de fome",
      link: "https://cetesc.com.br",
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuCz1wPGHtteKqifBJtudl_Tvu5EeGSTY3iw&usqp=CAU",
    }
  ]
  return (
    <div>
      <div className="section text-center bg-warning text-dark mt-2">
        <div className="container">
          <div className="row">
            <div className="col">
              <h1>Seja Bem Vindo</h1>
            </div>
          </div>
        </div>
      </div>

      <div className="mt-2">
        <Carousel />
      </div>

      <div id="list-example" className="list-group">
        <a className="list-group-item list-group-item-action" href="#list-item-1">Item 1</a>
        <a className="list-group-item list-group-item-action" href="#list-item-2">Item 2</a>
        <a className="list-group-item list-group-item-action" href="#list-item-3">Item 3</a>
        <a className="list-group-item list-group-item-action" href="#list-item-4">Item 4</a>
      </div>
      <div data-spy="scroll" data-target="#list-example" data-offset="0" className="scrollspy-example">
        <h4 id="list-item-1">Item 1</h4>
        <p>...</p>
        <h4 id="list-item-2">Item 2</h4>
        <p>...</p>
        <h4 id="list-item-3">Item 3</h4>
        <p>...</p>
        <h4 id="list-item-4">Item 4</h4>
        <p>...</p>
      </div>

      <div className="row p-3">
        {dados.map(item => (
          <div className="col-3" key={item.id}>
            <Card title={item.title} body={item.body} link={item.link} img={item.img} />
          </div>
        ))}
      </div>
    </div>
  )
}

export default Home