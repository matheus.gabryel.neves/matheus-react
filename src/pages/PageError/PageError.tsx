import styled from "styled-components";

const Error = () => {
  return (
    <Container className="container">
      <div className="row justify-content-center">
        <div className="col-sm col-lg-8">
          <div className="card text-center">

            <h5 className="card-header bg-secondary text-light">Página Não Encontrada</h5>

            <div className="card-body bg-dark text-white text-center py-5">
              <h5 className="card-title">Tente Novamente!</h5>
              <p className="card-text">Retornando ao Menu Inicial</p>
              <a href="/" className="btn btn-warning">Home</a>
            </div>
          </div>
        </div>
      </div>
    </Container>
  )
}

export default Error


export const Container = styled.div`
  height: 100vh;

  .card {
    margin-top: 30%;
  }
`