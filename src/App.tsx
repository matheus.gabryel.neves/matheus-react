
import './App.css';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer';
import { BrowserRouter } from "react-router-dom";
import Routes from "./routes"

export function App() {

  return (
    <BrowserRouter>
      <div>
        <div>
          <Navbar />
        </div>
        <Routes />
        <div>
          <Footer />
        </div>

      </div>
    </BrowserRouter>
  );
}
