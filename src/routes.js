import { Route, Switch } from "react-router-dom";

import Home from "./pages/Home";
import Error from "./pages/PageError";
import Sobre from "./pages/Sobre";

const Routes = () => {
   return(
       <Switch>
           <Route exact component = { Home }  path="/" />
           <Route  component = { Sobre }  path="/sobre" />
           <Route  component = { Error }  path="*" />
       </Switch>
   )
}

export default Routes;